#!/usr/bin/env bash
#PBS -W group_list=cu_10145 -A cu_10145
### Job name (comment out the next line to get the name of the script used as the job name)
#PBS -N CNACS
### Output files (comment out the next 2 lines to get the job name used instead)
#PBS -e test.err
#PBS -o test.log
### Only send mail when job is aborted or terminates abnormally
#PBS -m n
### Number of nodes
#PBS -l nodes=1:ppn=30  # ppn=30 for 30 jobs w parallel?
### Memory
#PBS -l mem=350gb   # Q: is this per CPU or total?
### Requesting time - format is <days>:<hours>:<minutes>:<seconds> (here, 12 hours)
#PBS -l walltime=99:00:00   
### Forward X11 connection (comment out if not needed)
###PBS -X
  
# Go to the directory from where the job was submitted (initial directory is $HOME)
echo Working directory is $PBS_O_WORKDIR
cd $PBS_O_WORKDIR
 
### Here follows the user commands:
# Define number of processors
NPROCS=`wc -l < $PBS_NODEFILE`
echo This job has allocated $NPROCS nodes
 
# Load all required modules for the job

. ./cnacs_env.sh

#set +eu
#eval "$(conda shell.bash hook)"
#module load /home/projects/cu_10184/people/frakan/projects/pth_CMP001-CMP014/code/load_modules.txt
#conda activate /home/projects/cu_10184/people/frakan/conda/py27
#set -ue
#
##module load toil_cnacs/0.2.0
#
##
#
#
#OUT_DIR=./cnacs_out2
#DIR_BAMS=/home/projects/cu_10184/people/frakan/projects/PTH/data/bam
#POOL_DIR=$OUT_DIR/pool
#TMPDIR=$OUT_DIR/tmp
#RUN_DIR=$OUT_DIR/run
#
##export REF_GEN=/home/francisc/data/refs/hs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly_chr.fa
#export DB_HG38="/home/projects/cu_10184/people/frakan/projects/cnacs/db"
#export TMPDIR
#export _JAVA_OPTIONS=-Djava.io.tmpdir=$TMPDIR
#export BEDTOOLS_PATH="$(dirname $(which bedtools))"
#export BEDTOOLS_PATH="/home/projects/cu_10184/people/frakan/conda/py27/bin"
#export SAMTOOLS_PATH="$(dirname $(which samtools))"
#export JAVAPATH="$(dirname $(which java))" #/usr/lib/jvm/java-1.8.0/bin   #/usr/lib/jvm/default-java/bin
#export PICARD_PATH="/home/projects/cu_10184/people/frakan/conda/py27/share/picard-2.7.1-2/picard.jar"
#export PERL_PATH="$(which perl)"
#
#export R_PATH=/home/projects/cu_10184/people/frakan/conda/py2/bin/R
#export R_LIBS=/home/projects/cu_10184/people/frakan/conda/py2/lib/R/library
#export R_LIBS_PATH=/home/projects/cu_10184/people/frakan/conda/py2/lib/R/library
#
#
#export REF_GEN=/home/projects/cu_10184/people/frakan/refs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly_chr.fa
#export REGIONS=/home/projects/cu_10184/people/frakan/projects/pth_panel_seq_pipeline/meta/probes_combined_PTH_Comprehensive_Myeloid_Panel_v2_2_TE-98216079_hg38_with-NPM1-ins-probes_chr_clean.bed
#export SAMPLE_INFO=/home/projects/cu_10184/people/frakan/projects/pth_panel_seq_pipeline/meta/sample_info.tsv
#export DB_HG38=/home/projects/cu_10184/people/frakan/projects/cnacs/db
#

#
# ********************** CNACS Step 1: Generate Pool of Normals **********************
#

echo
echo "## $(date "+%H:%M:%S:") CNACS Step 1: Generate Pool of Normals..."
echo



#
# build command
#
CMD_GEN_POOL="env BEDTOOLS_PATH=/home/projects/cu_10184/people/frakan/conda/py27/bin
toil_cnacs generate_pool \
$POOL_DIR/jobstore_generate_pool \
--stats \
--writeLogs $POOL_DIR/toil_logs \
--logFile $POOL_DIR/toil_logs.txt \
--outdir $POOL_DIR \
--probe_bed $REGIONS \
--fasta $REF_GEN \
--db_dir $DB_HG38 "

CMD_GEN_POOL="$CMD_GEN_POOL "$(grep "CMP02[01].*NORMAL" $SAMPLE_INFO | awk -v DIR_BAMS=$DIR_BAMS '{print "--pool_samp ", DIR_BAMS"/"$1"*.bam", "F"}' | tr "\n" " ")


echo "$CMD_GEN_POOL" | tee tmp.txt
$CMD_GEN_POOL
