Running toil_cnacs
================

It requires some tinkering to set it up. This is to be run on Computerome 2

- clone [toil_cnacs](https://github.com/papaemmelab/toil_cnacs)
- install the following modules and conda environment (python 2.7):


        module load tools ngs
        module load anaconda3/4.4.0
        module load java/1.8.0 jre/1.8.0
   
        conda create --prefix cnacs_env python=2.7 r r-remotes r-dppackage bioconductor-dnacopy picard=2.7.1 perl bedtools samtools


- under the toil_cnacs/ directory run

        pip install .

- replace db/ with the hg38 version
- make sure probe regions and the reference genome chromosomes start with “chr”
- adjust all the variables in cnacs_env.sh to refer to your local setup


          OUT_DIR=./cnacs_out2
          DIR_BAMS=/home/projects/cu_10184/people/frakan/projects/PTH/data/bam
          POOL_DIR=$OUT_DIR/pool
          TMPDIR=$OUT_DIR/tmp
          RUN_DIR=$OUT_DIR/run

          #export REF_GEN=/home/francisc/data/refs/hs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly_chr.fa
          export DB_HG38="/home/projects/cu_10184/people/frakan/projects/cnacs/db"
          export TMPDIR
          export _JAVA_OPTIONS=-Djava.io.tmpdir=$TMPDIR
          export BEDTOOLS_PATH="$(dirname $(which bedtools))"
          export BEDTOOLS_PATH="/home/projects/cu_10184/people/frakan/conda/py27/bin"
          export SAMTOOLS_PATH="$(dirname $(which samtools))"
          export JAVAPATH="$(dirname $(which java))"
          export PICARD_PATH="/home/projects/cu_10184/people/frakan/conda/py27/share/picard-2.7.1-2/picard.jar"
          export PERL_PATH="$(which perl)"

          export R_PATH=/home/projects/cu_10184/people/frakan/conda/py2/bin/R
          export R_LIBS=/home/projects/cu_10184/people/frakan/conda/py2/lib/R/library
          export R_LIBS_PATH=/home/projects/cu_10184/people/frakan/conda/py2/lib/R/library


          export REF_GEN=/home/projects/cu_10184/people/frakan/refs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly_chr.fa
          export REGIONS=/home/projects/cu_10184/people/frakan/projects/pth_panel_seq_pipeline/meta/probes_combined_PTH_Comprehensive_Myeloid_Panel_v2_2_TE-98216079_hg38_with-NPM1-ins-probes_chr_clean.bed
          export SAMPLE_INFO=/home/projects/cu_10184/people/frakan/projects/pth_panel_seq_pipeline/meta/sample_info.tsv
          export DB_HG38=/home/projects/cu_10184/people/frakan/projects/cnacs/db


The file *sample_info.tsv* must have the sample names in the 1st column and a column indicating which samples are NORMAL (control) samples and which are not (patients/treatment).



## Generating a Panel of Normals (done 1x)

- step 1: run

        bash ./toil_cnacs_step1_dispatch.sh


- step 2: finalize pool of normals

        bash toil_cnacs_step2_dispatch.sh
       
      
    if this fails, adjust the "thresholds.txt" file under cnacs_out/pool/stats/ according to the histogram plots



## Call CNVs

To call CNVs for each sample in the sample_info.tsv file, run

         bash toil_cnacs_step3_call_CNVs.sh

