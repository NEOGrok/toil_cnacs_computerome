#!/usr/bin/env bash


. ./cnacs_env.sh

cut -f1 $SAMPLE_INFO | while read s; do
        echo -e "${s}\t$DIR_BAMS/${s}*_bqsr.bam"
        qsub -v SAMPLE=$s,OUT_BAM="$DIR_BAMS/${s}*_bqsr.bam" toil_cnacs_step3.sh && sleep 1
done




