
#
# ********************** CNACS Env Setup: Manual **********************
#


# Load all required modules for the job


set +eu
eval "$(conda shell.bash hook)"
module load /home/projects/cu_10184/people/frakan/projects/pth_CMP001-CMP014/code/load_modules.txt
conda activate /home/projects/cu_10184/people/frakan/conda/py27
set -ue

#module load toil_cnacs/0.2.0


OUT_DIR=./cnacs_out2
DIR_BAMS=/home/projects/cu_10184/people/frakan/projects/PTH/data/bam
POOL_DIR=$OUT_DIR/pool
TMPDIR=$OUT_DIR/tmp
RUN_DIR=$OUT_DIR/run

#export REF_GEN=/home/francisc/data/refs/hs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly_chr.fa
export DB_HG38="/home/projects/cu_10184/people/frakan/projects/cnacs/db"
export TMPDIR
export _JAVA_OPTIONS=-Djava.io.tmpdir=$TMPDIR
export BEDTOOLS_PATH="$(dirname $(which bedtools))"
export BEDTOOLS_PATH="/home/projects/cu_10184/people/frakan/conda/py27/bin"
export SAMTOOLS_PATH="$(dirname $(which samtools))"
export JAVAPATH="$(dirname $(which java))" #/usr/lib/jvm/java-1.8.0/bin   #/usr/lib/jvm/default-java/bin
export PICARD_PATH="/home/projects/cu_10184/people/frakan/conda/py27/share/picard-2.7.1-2/picard.jar"
export PERL_PATH="$(which perl)"

export R_PATH=/home/projects/cu_10184/people/frakan/conda/py2/bin/R
export R_LIBS=/home/projects/cu_10184/people/frakan/conda/py2/lib/R/library
export R_LIBS_PATH=/home/projects/cu_10184/people/frakan/conda/py2/lib/R/library


export REF_GEN=/home/projects/cu_10184/people/frakan/refs/genome/ensembl/chr_only/Homo_sapiens.GRCh38_chrOnly_chr.fa
export REGIONS=/home/projects/cu_10184/people/frakan/projects/pth_panel_seq_pipeline/meta/probes_combined_PTH_Comprehensive_Myeloid_Panel_v2_2_TE-98216079_hg38_with-NPM1-ins-probes_chr_clean.bed
export SAMPLE_INFO=/home/projects/cu_10184/people/frakan/projects/pth_panel_seq_pipeline/meta/sample_info.tsv
export DB_HG38=/home/projects/cu_10184/people/frakan/projects/cnacs/db

